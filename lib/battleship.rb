class BattleshipGame
  attr_accessor :board, :player, :board_total

  def initialize(player, board)
    @player = player
    @board = board.grid
    @board_total = board
  end

  def attack(pos)


    x = pos[0]
    y = pos[1]

    if board[x][y] == :s
      board[x][y] = :x
      puts "It's a hit!"
    elsif board[x][y] == :x
      puts 'You already attacked this position!'
    elsif board[x][y] == nil
      board[x][y] = :x
      puts "Just water.."
    end
  end

  def play_turn
    puts "Where would you like to attack? ex. 0, 0"
    pos = player.get_play
    attack(pos)
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

end
