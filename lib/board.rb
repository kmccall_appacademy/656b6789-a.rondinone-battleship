class Board

  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
      ships = 0
      @grid.flatten.each { |i| ships += 1 if i != nil }
      ships
    end

  def empty?(position = nil)
    if position == nil && count > 0
      return false
    elsif position == nil && count == 0
      return true
    end

    return self.grid[position[0]][position[1]] == nil
  end

  def full?
    return false if @grid.flatten.any? { |i| i == nil }
    true
  end

  def place_random_ship
    raise if self.full?

    @grid[random_pos][random_pos] = :s if @grid[random_pos][random_pos] == nil
   end

   def random_pos
     rand(grid.length)
   end

   def won?
    @grid.flatten.none? { |i| i == :s }
   end



end
