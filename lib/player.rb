class HumanPlayer

  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play
    gets.chomp.delete(',').split.map(&:to_i)
    # [1, 1]
  end

end
